'use strict';

(function() {

class AdminController {
  constructor() {

  }
}

angular.module('incognitoApp.admin')
  .controller('AdminController', AdminController);

})();
