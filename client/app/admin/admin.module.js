'use strict';

angular.module('incognitoApp.admin', [
  'incognitoApp.auth',
  'ui.router'
]);
