'use strict';

(function() {

  class EventTypesCtontroller {
    constructor($http, socket, $scope) {
      // Use the User $resource to fetch all users
      this.$http = $http;
      this.new_event = {};
      this.eventTypes = [];

      $http.get('/api/eventTypes').then(response=>{
        this.eventTypes = response.data;

        socket.syncUpdates('eventType', this.eventTypes);
      });

      $scope.$on('$destroy', function() {
        socket.unsyncUpdates('eventType');
      });
    }

    addEventType(){
      this.$http.post('/api/eventTypes', this.new_event);
    }
  }

  angular.module('incognitoApp.admin')
      .controller('EventTypesCtrl', EventTypesCtontroller);

})();
