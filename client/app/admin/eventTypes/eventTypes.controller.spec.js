'use strict';

describe('Controller: EventTypesCtrl', function () {

  // load the controller's module
  beforeEach(module('incognitoApp'));

  var EventTypesCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventTypesCtrl = $controller('EventTypesCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
