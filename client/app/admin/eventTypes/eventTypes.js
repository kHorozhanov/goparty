'use strict';

angular.module('incognitoApp.admin')
    .config(function ($stateProvider) {
        $stateProvider
            .state('eventTypes', {
                parent: 'admin',
                url: '/eventTypes',
                templateUrl: 'app/admin/eventTypes/eventTypes.html',
                controller: 'EventTypesCtrl',
                controllerAs: 'types'
            });
    });
