'use strict';

angular.module('incognitoApp.admin')
    .config(function ($stateProvider) {
        $stateProvider
            .state('users', {
                parent: 'admin',
                url: '/users',
                templateUrl: 'app/admin/users/users.html',
                controller: 'UsersCtrl',
                controllerAs: 'users'
            });
    });
