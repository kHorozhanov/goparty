(function(angular, undefined) {
'use strict';

angular.module('incognitoApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);