'use strict';

angular.module('incognitoApp', [
  'incognitoApp.auth',
  'incognitoApp.admin',
  'incognitoApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'validation.match',
  'ngMaterial',
  'uiGmapgoogle-maps'
])
  .config(function ($urlRouterProvider, $mdThemingProvider) {
    $urlRouterProvider
      .otherwise('/');
    $mdThemingProvider.theme('default')
      .primaryPalette('teal', {
        'default': '500', // by default use shade 400 from the pink palette for primary intentions
        'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
        'hue-2': '700', // use shade 600 for the <code>md-hue-2</code> class
        'hue-3': 'A100'
      })
      .accentPalette('brown')
      .backgroundPalette('grey', {
        'default': '100'
      });
  });
