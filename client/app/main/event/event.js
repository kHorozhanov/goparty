'use strict';

(function() {

class EventCtrl {
  constructor($http, $scope, $mdSidenav, $state, $mdComponentRegistry, $stateParams, Auth, $mdDialog, socket) {
   this.$state = $state;
   this.isOpened = true;
   this.$http = $http;
   this.imgSrc;
   this.creator;
   this.user = Auth.getCurrentUser();
   this.$mdDialog = $mdDialog;

    $scope.$watch(() => this.isOpened, (newValue) => {
    	if (newValue == false){
    		this.$state.go('main');
    	}
    });

    $http.get('/api/events/' + $stateParams.id)
    	.then(success => {
    		this.data = success.data;
    		console.log(this.data);
    		this.getuserImg(this.data);
    		this.getAddress();

    	}, error => {

    	});

    $http.get('/api/events/' + $stateParams.id + '/users')
      .then(success => {
        this.users = success.data;
        console.log(this.users);
      }, error => {

      });

    this.items = [
        { name: "Twitter", icon: "fa fa-twitter", direction: "bottom" },
        { name: "Facebook", icon: "fa fa-facebook", direction: "top" },
        { name: "Vkontakte", icon: "fa fa-vk", direction: "bottom" }
      ];
  }

  closeSideNav(){
  	this.isOpened = false;
    this.$state.go('main');
  }

  joinToEvent(){
  	//api/events/connect + event_id
  	console.log(this.user);
  	var inEvent;
  	var idArrays = [];

  	if (this.user._id == this.data.user._id){
  		this.$mdDialog.show(
	      this.$mdDialog.alert()
	        .parent(angular.element(document.querySelector('#modal_parent')))
	        .clickOutsideToClose(true)
	        .title('Ошибка')
	        .textContent('Вы являетесь создателем этого события')
	        .ariaLabel('Ошибка')
	        .ok('Ок')
	    );
  		return false;
  	}

  	this.users.forEach((value) => {
  		if (this.user._id == value.user._id){
  			inEvent = true;
  		}
  	});

  	if (!inEvent){
  		this.$http.post('/api/userToEvents/', {
        user: this.user._id,
        event: this.data._id
      }).
  			then((response) => {
          response.data.user = this.user;
  				this.users.push(response.data);
  				this.$mdDialog.show(
			      this.$mdDialog.alert()
			        .parent(angular.element(document.querySelector('#modal_parent')))
			        .clickOutsideToClose(true)
			        .title('Результат')
			        .textContent('Вы вступили в событие ' + this.data.name)
			        .ariaLabel('Результат')
			        .ok('Ок')
			    );
  			}, (error) => {
  				alert('SOMETHING WENT WRONG...');
  			})
  	} else{
  		this.$mdDialog.show(
	      this.$mdDialog.alert()
	        .parent(angular.element(document.querySelector('#modal_parent')))
	        .clickOutsideToClose(true)
	        .title('Ошибка')
	        .textContent('Вы являетесь участником данного события')
	        .ariaLabel('Ошибка')
	        .ok('Ок')
	    );
  	}
  }

  getuserImg(data){
  	switch(data.user.provider){
  		case 'google':
  			this.imgSrc = data.user.google.image.url;
  			break
  		case 'facebook':
  			this.imgSrc = 'https://graph.facebook.com/' + data.user.facebook.id +'/picture?height=100';
  			break
  		case 'twitter':
  			this.imgSrc = data.user.twitter.profile_image_url_https;
  		case 'local':
  			break
  	}
  }

  getAddress(){
  	this.$http.get('https://maps.googleapis.com/maps/api/geocode/json?&latlng=' + this.data.latitude + ',' + this.data.longitude).
  		then((response) => {
  			this.data.formatted_address = response.data.results[0].formatted_address;
  		})
  }

}

angular.module('incognitoApp')
  .controller('EventCtrl', EventCtrl);

})();
