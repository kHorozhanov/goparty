'use strict';

(function() {

class MainController {
  constructor($http, $scope, socket, $timeout, $state) {
    this.$http = $http;
    this.events = [];
    this.event = {};
    this.displayed = false;
    this.resultsSearch = [];
    //this.selectedItem = {};
    this.search= {
      inProgress: false,
      needRequest: false
    };

    this.options = {
      window: {
        options: {
          minWidth: 500
        }
      }
    };

    $timeout(()=>{
      this.displayed = true;
    }, 100);

    this.$state = $state;
    this.markerEvents = {
      mouseover: (marker, eventName, model, args) => {
        model.show = true;
        console.log(model);
      },
      mouseout: (marker, eventName, model, args) => {
        model.show = false;
      },
      click: (marker, eventName, model, args) => {
        this.$state.go('main.event', {id: model._id});
      }
    };


    this.map = {
      center: {
        latitude: 50.448734,
        longitude: 30.528724
      },
      zoom: 12,
      refresh: false
    };

    navigator.geolocation.getCurrentPosition(position => {
      this.$http.get('https://maps.googleapis.com/maps/api/geocode/json?&latlng=' + position.coords.latitude + ',' + position.coords.longitude).then(response=>{
        this.selectedItem = response.data.results[0];
        this.findEvents();
      });
    });

    $scope.$watchCollection(() => {
      return this.selectedItem;
    }, ()=>{
      if(this.selectedItem){
        this.map.center.latitude = this.selectedItem.geometry.location.lat;
        this.map.center.longitude = this.selectedItem.geometry.location.lng;
        this.event.latitudeMin = this.selectedItem.geometry.viewport.southwest.lat;
        this.event.latitudeMax = this.selectedItem.geometry.viewport.northeast.lat;
        this.event.longitudeMin = this.selectedItem.geometry.viewport.southwest.lng;
        this.event.longitudeMax = this.selectedItem.geometry.viewport.northeast.lng;
      }

    });

    $http.get('/api/eventTypes').then(response =>{
      this.types = response.data;
    });

    //$http.get('/api/events').then(response => {
    //  this.events = response.data;
    //});

  }


  searchTextChange(text){
    if(text && !this.search.inProgress){
      this.search.inProgress = true;
      this.$http.get('https://maps.googleapis.com/maps/api/geocode/json?&address=' + text).then(response=>{
        this.resultsSearch = response.data.results;
        this.search.inProgress = false;
        if(this.search.needRequest){
          this.search.needRequest = false;
          this.searchTextChange(this.searchText);
        }
      });
    }else if(text && this.search.inProgress){
      this.search.needRequest = true;
    }
  }

  findEvents(){
    var query = '?';
    angular.forEach(this.event, (value, key)=>{
      query += key + '=' + value + '&';
    });
    this.$http.get('/api/events' + query).then(response=>{
      this.events = response.data;
    })
  }

  selectedItemChange(item){
    this.selectedItem = item;
    //this.findEvents();
  }
}

angular.module('incognitoApp')
  .controller('MainController', MainController);
})();
