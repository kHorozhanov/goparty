'use strict';

angular.module('incognitoApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            })

            .state('new_event', {
                url: '/new',
                authenticate: 'user',
                controller: 'NewEventCtrl',
                controllerAs: 'create',
                templateUrl: 'app/main/new_event/new_event.html'
            })
            .state('main.event', {
                url: 'event/:id',
                parent: 'main',
                templateUrl: 'app/main/event/event.html',
                controller: 'EventCtrl',
                controllerAs: 'event'
            })
    });
