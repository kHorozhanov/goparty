'use strict';

(function() {

class NewEventCtrl {
  constructor($http, $scope, $timeout, Auth, $mdDialog, $mdMedia, socket) {
    this.user = Auth.getCurrentUser();
    this.http = $http;
    this.types = [];
    this.search= {
      inProgress: false,
      needRequest: false
    };
    this.$mdDialog = $mdDialog;
    this.response;

    this.event = {};
    this.resultsSearch = [];
    this.displayed = false;
    $timeout(()=>{
      this.displayed = true;
    }, 100);
    this.event.date_start = new Date();
    this.event.date_end = new Date();
    this.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

    this.map = {
      center: {
        latitude: 50.448734,
        longitude: 30.528724
      },
      zoom: 12,
      refresh: false
    };

    $http.get('/api/eventTypes').then(response =>{
      this.types = response.data;
    });

    this.marker = {
      id: 0,
      options: { draggable: true },
      coords: {
        latitude: 50.448734,
        longitude: 30.528724
      },
      events: {
        dragend: (marker, eventName, args)=>{
          var lat = marker.getPosition().lat();
          var lon = marker.getPosition().lng();
          $http.get('https://maps.googleapis.com/maps/api/geocode/json?&latlng=' + lat + ',' + lon).then(response=>{
            this.selectedItem = response.data.results[0];
          });
        }
      }
    };

    navigator.geolocation.getCurrentPosition(position => {
      this.http.get('https://maps.googleapis.com/maps/api/geocode/json?&latlng=' + position.coords.latitude + ',' + position.coords.longitude).then(response=>{
        this.selectedItem = response.data.results[0];
      });
    });

    $scope.$watchCollection(() => {
      return this.selectedItem;
    }, ()=>{
      if(this.selectedItem){
        this.event.latitude = this.selectedItem.geometry.location.lat;
        this.event.longitude = this.selectedItem.geometry.location.lng;
        this.marker.coords.latitude = this.selectedItem.geometry.location.lat;
        this.marker.coords.longitude = this.selectedItem.geometry.location.lng;
        this.map.center.latitude = this.selectedItem.geometry.location.lat;
        this.map.center.longitude = this.selectedItem.geometry.location.lng;
      }

    });

  }

  searchTextChange(text){
    if(text && !this.search.inProgress){
      this.search.inProgress = true;
      this.http.get('https://maps.googleapis.com/maps/api/geocode/json?&address=' + text).then(response=>{
        this.resultsSearch = response.data.results;
        this.search.inProgress = false;
        if(this.search.needRequest){
          this.search.needRequest = false;
          this.searchTextChange(this.searchText);
        }
      });
    }else if(text && this.search.inProgress){
      this.search.needRequest = true;
    }
  }

  createEvent(){
  	console.log(this.event);
  	this.http.post('/api/events', this.event).
  		then(response => {
        console.log(response);
        this.response = response.data;
        this.$mdDialog.show({
            templateUrl: 'app/main/new_event/modal.html',
            parent: angular.element(document.querySelector('#modal_parent')),
            clickOutsideToClose: true,
            controller: ($scope, $state, $mdDialog) => {
              $scope.response = this.response;

              $scope.okey = (state) => {
                $state.go(state, {}, {reload: true});
                $mdDialog.cancel();
              }
            }
        }).then(submit => {
          console.log('submit')
        }, () => {
          console.log('NAH');
        })
  		}, error => {
  			alert('Something went wrong...');
  		});
  }

}

angular.module('incognitoApp')
  .controller('NewEventCtrl', NewEventCtrl);

})();
