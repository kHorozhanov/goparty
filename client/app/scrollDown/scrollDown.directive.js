'use strict';

angular.module('incognitoApp')
  .directive('scrollDown', function () {
    return {
      link: function (scope, element, attrs) {
        scope.$watch(
          function () {
            return element.children().length;
          },
          function () {
            console.log(element);
            element.scrollTop(element.prop('scrollHeight'));
          }
        );
      }
    };
  });
