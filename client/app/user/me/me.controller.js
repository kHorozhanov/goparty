'use strict';

(function() {

class MeCtrl {
  constructor($scope, $http, Auth) {

  	this.userInfo = {
  		politcsViews: [
	  		'Коммунизм',
	  		'Социализм',
	  		'Либерализм',
	  		'Консерватизм',
	  		'Монархизм',
	  		'Ультраконсерватизм',
	  		'Анархизм',
	  		'Национализм',
	  		'Не интересно'
	  	],
	  	gender: [
	  		'Мужской',
	  		'Женский'
	  	],
	  	religion: [
	  		'Иудаизм',
	  		'Католицизм',
	  		'Протестанство',
	  		'Ислам',
	  		'Буддизм',
	  		'Конфуциизм',
	  		'Православие'
	  	],
	  	negative: [
	  		'Сильно негативное',
	  		'Негативное',
	  		'Компромиссное',
	  		'Нейтральное',
	  		'Позитивное'
	  	],
	  	chips: []
  	}

  	this.user = {
  		gender: '',
  		age: '',
  		hometown: '',
  		currentCity: '',
  		politics: '',
  		religion: '',
  		alcohol: '',
  		smoking: '',
  		job: '',
  		info: '',
  		positiveKeyWords: []
  	}
  }

}

angular.module('incognitoApp')
  .controller('MeCtrl', MeCtrl);

})();

