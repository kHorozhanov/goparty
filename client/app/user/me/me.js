'use strict';

angular.module('incognitoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('me', {
        parent: 'user',
        url: '/me',
        templateUrl: 'app/user/me/me.html',
        controller: 'MeCtrl',
        controllerAs: 'me',
        authenticate: 'user'
      });
  });
