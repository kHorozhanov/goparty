'use strict';

(function () {

  class MyEventsCtrl {
    constructor($scope, $http, Auth, socket) {

      this.eventsList = {};
      this.imgSrc;

      $http.get('/api/events/my/created').
        then((response) => {
          this.eventsList.created_events = response.data;
          console.log(this.eventsList);
          //socket.syncCreatedEvents('event', this.eventsList.created_events, Auth.getCurrentUser());


        });

      $http.get('/api/events/my/confirmed/1').
        then((response) => {
          this.eventsList.confirm_events = response.data;
          console.log(this.eventsList);
          //socket.syncCreatedEvents('event', this.eventsList.created_events, Auth.getCurrentUser());


        });

      $http.get('/api/events/my/confirmed/0').
        then((response) => {
          this.eventsList.events = response.data;
          console.log(this.eventsList);
          //socket.syncCreatedEvents('event', this.eventsList.created_events, Auth.getCurrentUser());
        });

    

      $scope.$on('$destroy', function () {
        socket.unsyncUpdates('event');
      });
    }
  }

  angular.module('incognitoApp')
    .controller('MyEventsCtrl', MyEventsCtrl);

})();
