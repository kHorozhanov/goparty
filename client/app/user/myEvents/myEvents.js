'use strict';

angular.module('incognitoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('myEvents', {
        parent: 'user',
        url: '/events',
        templateUrl: 'app/user/myEvents/myEvents.html',
        controller: 'MyEventsCtrl',
        controllerAs: 'events',
        authenticate: true
      });
  });
