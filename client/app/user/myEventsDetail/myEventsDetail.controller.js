'use strict';

(function () {


	class MyEventsDetailCtrl {
		eventdetail;
		imgSrc;
		users;
		messages;
		message;
		currentUser;

		constructor($scope, $http, $stateParams, $mdToast, Auth, socket, userStatus) {
			this.$http = $http;
			this.$mdToast = $mdToast;
			this.Auth = Auth;
			this.currentUser = Auth.getCurrentUser();
			this.userStatus = userStatus;
			
			console.log(userStatus);

			$http.get('/api/events/' + $stateParams.id).
				then((response) => {
					this.eventdetail = response.data;
					this.getAddress();
					this.getuserImg(this.eventdetail.user);
					console.log(this.eventdetail);
				});

			$http.get('/api/events/' + $stateParams.id + '/users').
				then(response => {
					console.log(response.data);
					this.users = response.data;
				}, error => {

				});
			$http.get('/api/massages/' + $stateParams.id).
				then((response) => {
					this.messages = response.data;
					console.log(this.messages);
					socket.syncMessages('massage', this.messages, $stateParams.id)
				}, (err) => {
					console.error(err);
				});

			$scope.$on('$destroy', function () {
				socket.unsyncUpdates('massage');
			});
		}

		getAddress() {
			this.$http.get('https://maps.googleapis.com/maps/api/geocode/json?&latlng=' + this.eventdetail.latitude + ',' + this.eventdetail.longitude).
				then((response) => {
					this.eventdetail.formatted_address = response.data.results[0].formatted_address;
				})
		}

		sendMessage() {
			if (this.message.length) {
				this.$http.post('/api/massages', {
					text: this.message,
					user: this.currentUser,
					event: this.eventdetail
				}).then((response) => {
					this.message = '';
				})
			}

		}

		getuserImg(data) {
			switch (data.provider) {
				case 'google':
					this.imgSrc = data.google.image.url;
					break
				case 'facebook':
					this.imgSrc = 'https://graph.facebook.com/' + data.facebook.id + '/picture?height=100';
					break
				case 'twitter':
					this.imgSrc = data.twitter.profile_image_url_https;
				case 'local':
					break
			}
		}

		acceptUser(user) {
			user.status = !user.status;
			this.$http.put('/api/userToEvents/' + user._id, user).
				then((response) => {
					this.$mdToast.show(
						this.$mdToast.simple()
							.textContent('Статус пользователя изменен!')
							.position('bottom right')
							.hideDelay(3000)
					);
				}, (error) => {
					user.status = !user.status;
				})
		}
	}

	angular.module('incognitoApp')
		.controller('MyEventsDetailCtrl', MyEventsDetailCtrl);

})();
