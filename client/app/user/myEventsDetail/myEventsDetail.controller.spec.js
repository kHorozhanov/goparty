'use strict';

describe('Controller: MyEventsDetailCtrl', function () {

  // load the controller's module
  beforeEach(module('incognitoApp'));

  var MyEventsDetailCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MyEventsDetailCtrl = $controller('MyEventsDetailCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
