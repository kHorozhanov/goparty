'use strict';

angular.module('incognitoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('myEventsDetail', {
        parent: 'user',
        url: '/events/:id',
        templateUrl: 'app/user/myEventsDetail/myEventsDetail.html',
        controller: 'MyEventsDetailCtrl',
        controllerAs: 'detail',
        authenticate: true,
        resolve: {
          userStatus: (Auth, $http, $stateParams, $q) => {
            var deferred = $q.defer();
            var currentUser =  Auth.getCurrentUser(null);

            currentUser.then((user) => {
              $http.get('/api/userToEvents/check/' + user._id + '/' + $stateParams.id)
                .then((response) => {
                  deferred.resolve(response.data);
                }, (err) => {
                  deferred.reject(err);
                })
            })

            return deferred.promise;
          }
        }
      });
  });
