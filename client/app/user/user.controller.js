'use strict';

(function() {

class UserCtrl {
  constructor($scope, $http, Auth) {
  	
  	this.user = {};
  	this.imgSrc;

  	$http.get('/api/users/me').
  		then((response) => {
  			this.user = response.data;
  			console.log(this.user);
  			this.getUserImg(this.user);
  		})

  }

  getUserImg(user){
  	switch(user.provider){
  		case 'google':
  			user.google.image.url = user.google.image.url.replace('sz=50', 'sz=150');
  			this.imgSrc = user.google.image.url;
  			break
  		case 'facebook':
  			this.imgSrc = 'https://graph.facebook.com/' + user.facebook.id +'/picture?height=100';
  			break
  		case 'twitter':
  			this.imgSrc = user.twitter.profile_image_url_https;
  		case 'local':
  			break
  		default:
  			console.log('ne pruyshov');
  	}
  }
}

angular.module('incognitoApp')
  .controller('UserCtrl', UserCtrl);

})();

