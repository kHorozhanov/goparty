'use strict';

angular.module('incognitoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('user', {
        abstract: true,
        templateUrl: 'app/user/user.html',
        controller: 'UserCtrl',
        controllerAs: 'user',
        authenticate: true
      });
  });
