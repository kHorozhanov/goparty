'use strict';

angular.module('incognitoApp.auth', [
  'incognitoApp.constants',
  'incognitoApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
