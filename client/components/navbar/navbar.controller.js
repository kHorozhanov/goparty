'use strict';

class NavbarController {
    //start-non-standard
    menu = [
        {
            'title': 'Поиск событий',
            'state': 'main'
        },
        {
            'title': 'Создать событие',
            'state': 'new_event'
        }
    ];

    isCollapsed = true;
    //end-non-standard

    constructor(Auth) {
        this.isLoggedIn = Auth.isLoggedIn;
        this.isAdmin = Auth.isAdmin;
        this.getCurrentUser = Auth.getCurrentUser;
    }
}

angular.module('incognitoApp')
    .controller('NavbarController', NavbarController);
