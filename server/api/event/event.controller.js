/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/events              ->  index
 * POST    /api/events              ->  create
 * GET     /api/events/:id          ->  show
 * PUT     /api/events/:id          ->  update
 * DELETE  /api/events/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Event from './event.model';
import User from '../user/user.model';
import UserToEvent from '../userToEvent/userToEvent.model';

function populateUser(res) {
  return function (entity) {
    return Event.populate(entity, {path: 'user'});
  }
}
function populateUsers(res) {
  return function (entity) {
    return UserToEvent.populate(entity, {path: 'user'});
  }
}
function populateConfirmedUsers(res) {
  return function (entity) {
    return Event.populate(entity, {path: 'confirm_users'});
  }
}
function populateEvent_type(res) {
  return function (entity) {
    return Event.populate(entity, {path: 'eventType'});
  }
}
function populateEvent(res){
  return function (entity) {
    console.log(entity);
    return UserToEvent.populate(entity, {path: 'event', populate: {path: 'eventType'}});
  }
}
function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}


function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Events
export function index(req, res) {
  var query = {};
  if (req.query.latitudeMin && req.query.latitudeMax) {
    query.latitude = {
      "$gte": req.query.latitudeMin,
      "$lt": req.query.latitudeMax
    }
  }
  if (req.query.longitudeMin && req.query.longitudeMax) {
    query.longitude = {
      "$gte": req.query.longitudeMin,
      "$lt": req.query.longitudeMax
    }
  }
  if (req.query.name) {
    query.name = req.query.name
  }
  if (req.query.eventType) {
    query.eventType = req.query.eventType
  }
  if (req.query.user) {
    query.user = req.query.user
  }
  Event.findAsync(query)
    .then(populateUser(res))
    .then(populateEvent_type(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function myEvents(req, res) {
  var userId = req.user._id;
  Event.findAsync({user: userId})
      .then(populateEvent_type(res))
      .then(respondWithResult(res))
      .catch(handleError(res));
}
export function confirmed(req, res) {
  var userId = req.user._id;
  var status = Boolean(Number(req.params.status));
  console.log(status)
  UserToEvent.findAsync({user: userId, status: status})
    .then(populateEvent(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}
// Gets a single Event from the DB
export function show(req, res) {
  Event.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(populateUser(res))
    //.then(populateUsers(res))
    //.then(populateConfirmedUsers(res))
    .then(populateEvent_type(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function showUsers(req, res) {
  UserToEvent.findAsync({event: req.params.id})
      .then(handleEntityNotFound(res))
      .then(populateUsers(res))
      .then(respondWithResult(res))
      .catch(handleError(res));
}

// Creates a new Event in the DB
export function create(req, res) {
  var event = req.body;
  event.user = req.user._id;
  console.log(req.body);
  Event.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Event in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Event.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Updates an existing Event in the DB
//export function connect(req, res) {
//  Event.findByIdAsync(req.params.id)
//    .then(handleEntityNotFound(res))
//    .then(addUser(req.user._id))
//    .then(addEventToUser(req.user._id))
//    .then(respondWithResult(res))
//    .catch(handleError(res));
//}

// Deletes a Event from the DB
export function destroy(req, res) {
  Event.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
