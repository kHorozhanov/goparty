'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var User = require('../user/user.model');
var EventType = require('../event_type/event_type.model');

var EventSchema = new mongoose.Schema({
  name: String,
  info: String,
  eventType: {type:mongoose.Schema.Types.ObjectId, ref:'EventType'},
  active: Boolean,
  latitude: Number,
  longitude: Number,
  user: {type:mongoose.Schema.Types.ObjectId, ref:'User'},
  date_start: Date,
  date_end: Date
});

export default mongoose.model('Event', EventSchema);
