'use strict';

var express = require('express');
var controller = require('./event.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/:id/users', controller.showUsers);
router.get('/my/created', auth.isAuthenticated(), controller.myEvents);
router.get('/my/confirmed/:status', auth.isAuthenticated(), controller.confirmed);
router.post('/', auth.isAuthenticated(), controller.create);
//router.post('/connect/:id', auth.isAuthenticated(), controller.connect);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
