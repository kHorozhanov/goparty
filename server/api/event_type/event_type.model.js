'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var Event = require('../event/event.model');

var EventTypeSchema = new mongoose.Schema({
  name: String,
  info: String,
  img: String,
  link: '',
  events: [{type:mongoose.Schema.Types.ObjectId, ref:'Event'}]
});

export default mongoose.model('EventType', EventTypeSchema);
