'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var massageCtrlStub = {
  index: 'massageCtrl.index',
  show: 'massageCtrl.show',
  create: 'massageCtrl.create',
  update: 'massageCtrl.update',
  destroy: 'massageCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var massageIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './massage.controller': massageCtrlStub
});

describe('Massage API Router:', function() {

  it('should return an express router instance', function() {
    massageIndex.should.equal(routerStub);
  });

  describe('GET /api/massages', function() {

    it('should route to massage.controller.index', function() {
      routerStub.get
        .withArgs('/', 'massageCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/massages/:id', function() {

    it('should route to massage.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'massageCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/massages', function() {

    it('should route to massage.controller.create', function() {
      routerStub.post
        .withArgs('/', 'massageCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/massages/:id', function() {

    it('should route to massage.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'massageCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/massages/:id', function() {

    it('should route to massage.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'massageCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/massages/:id', function() {

    it('should route to massage.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'massageCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
