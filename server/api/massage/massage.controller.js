/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/massages              ->  index
 * POST    /api/massages              ->  create
 * GET     /api/massages/:id          ->  show
 * PUT     /api/massages/:id          ->  update
 * DELETE  /api/massages/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Massage from './massage.model';
import User from '../user/user.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function populateUser(res) {
  return function (entity) {
    return Massage.populate(entity, {path: 'user'});
  }
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Massages
export function index(req, res) {
  Massage.findAsync({event: req.params.eventId})
    .then(populateUser(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Massage from the DB
// export function show(req, res) {
//   Massage.findByIdAsync(req.params.id)
//     .then(handleEntityNotFound(res))
//     .then(respondWithResult(res))
//     .catch(handleError(res));
// }

// Creates a new Massage in the DB
export function create(req, res) {
  Massage.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Massage in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Massage.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Massage from the DB
export function destroy(req, res) {
  Massage.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
