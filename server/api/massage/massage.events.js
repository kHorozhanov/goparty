/**
 * Massage model events
 */

'use strict';

import {EventEmitter} from 'events';
var Massage = require('./massage.model');
var MassageEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MassageEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Massage.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    MassageEvents.emit(event + ':' + doc._id, doc);
    MassageEvents.emit(event, doc);
  }
}

export default MassageEvents;
