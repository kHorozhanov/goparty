'use strict';

var app = require('../..');
import request from 'supertest';

var newMassage;

describe('Massage API:', function() {

  describe('GET /api/massages', function() {
    var massages;

    beforeEach(function(done) {
      request(app)
        .get('/api/massages')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          massages = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      massages.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/massages', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/massages')
        .send({
          name: 'New Massage',
          info: 'This is the brand new massage!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newMassage = res.body;
          done();
        });
    });

    it('should respond with the newly created massage', function() {
      newMassage.name.should.equal('New Massage');
      newMassage.info.should.equal('This is the brand new massage!!!');
    });

  });

  describe('GET /api/massages/:id', function() {
    var massage;

    beforeEach(function(done) {
      request(app)
        .get('/api/massages/' + newMassage._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          massage = res.body;
          done();
        });
    });

    afterEach(function() {
      massage = {};
    });

    it('should respond with the requested massage', function() {
      massage.name.should.equal('New Massage');
      massage.info.should.equal('This is the brand new massage!!!');
    });

  });

  describe('PUT /api/massages/:id', function() {
    var updatedMassage;

    beforeEach(function(done) {
      request(app)
        .put('/api/massages/' + newMassage._id)
        .send({
          name: 'Updated Massage',
          info: 'This is the updated massage!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedMassage = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMassage = {};
    });

    it('should respond with the updated massage', function() {
      updatedMassage.name.should.equal('Updated Massage');
      updatedMassage.info.should.equal('This is the updated massage!!!');
    });

  });

  describe('DELETE /api/massages/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/massages/' + newMassage._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when massage does not exist', function(done) {
      request(app)
        .delete('/api/massages/' + newMassage._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
