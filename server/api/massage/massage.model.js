'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var User = require('../user/user.model');
var Event = require('../event/event.model');

var MassageSchema = new mongoose.Schema({
  text: String,
  user: {type:mongoose.Schema.Types.ObjectId, ref:'User'},
  event: {type:mongoose.Schema.Types.ObjectId, ref:'Event'},
  date: {type: Date, default: new Date()}
});

export default mongoose.model('Massage', MassageSchema);
