/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var MassageEvents = require('./massage.events');
var Message = require('./massage.model');

// Model events to emit
var events = ['save', 'remove'];

export function register(socket) {
  // Bind model events to socket events
  for (var i = 0, eventsLength = events.length; i < eventsLength; i++) {
    var event = events[i];
    var listener = createListener('massage:' + event, socket);

    MassageEvents.on(event, listener);
    socket.on('disconnect', removeListener(event, listener));
  }
}


function createListener(event, socket) {
  return function(doc) {
    
    Message.populate(doc, {path: 'user'}, function (err, doc) {
      if (err) return false;
      socket.emit(event, doc);
    })
    
  };
}

function removeListener(event, listener) {
  return function() {
    MassageEvents.removeListener(event, listener);
  };
}
