'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var userToEventCtrlStub = {
  index: 'userToEventCtrl.index',
  show: 'userToEventCtrl.show',
  create: 'userToEventCtrl.create',
  update: 'userToEventCtrl.update',
  destroy: 'userToEventCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var userToEventIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './userToEvent.controller': userToEventCtrlStub
});

describe('UserToEvent API Router:', function() {

  it('should return an express router instance', function() {
    userToEventIndex.should.equal(routerStub);
  });

  describe('GET /api/userToEvents', function() {

    it('should route to userToEvent.controller.index', function() {
      routerStub.get
        .withArgs('/', 'userToEventCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/userToEvents/:id', function() {

    it('should route to userToEvent.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'userToEventCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/userToEvents', function() {

    it('should route to userToEvent.controller.create', function() {
      routerStub.post
        .withArgs('/', 'userToEventCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/userToEvents/:id', function() {

    it('should route to userToEvent.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'userToEventCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/userToEvents/:id', function() {

    it('should route to userToEvent.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'userToEventCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/userToEvents/:id', function() {

    it('should route to userToEvent.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'userToEventCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
