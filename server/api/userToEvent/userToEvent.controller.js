/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/userToEvents              ->  index
 * POST    /api/userToEvents              ->  create
 * GET     /api/userToEvents/:id          ->  show
 * PUT     /api/userToEvents/:id          ->  update
 * DELETE  /api/userToEvents/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import UserToEvent from './userToEvent.model';
import Event from '../event/event.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function checkEventOwner(params, res) {
  return function(entity) {
    if (!entity) {
      return Event.findOneAsync({_id: params.event, user: params.user})
      .then((event) => {
        if (!event) return {status: 'not found'}
        return {status: 'creator'};
      })
    }
    if (entity.status) return {status: 'confirmed'}
    return {status: 'unconfirmed'}
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of UserToEvents
export function index(req, res) {
  UserToEvent.findAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single UserToEvent from the DB
export function show(req, res) {
  UserToEvent.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function check(req, res) {
  var params = {user: req.params.user, event: req.params.event};
  UserToEvent.findOneAsync(params)
    .then(checkEventOwner(params, res))
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new UserToEvent in the DB
export function create(req, res) {
  UserToEvent.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing UserToEvent in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  UserToEvent.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a UserToEvent from the DB
export function destroy(req, res) {
  UserToEvent.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
