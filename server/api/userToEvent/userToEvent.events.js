/**
 * UserToEvent model events
 */

'use strict';

import {EventEmitter} from 'events';
var UserToEvent = require('./userToEvent.model');
var UserToEventEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
UserToEventEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  UserToEvent.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    UserToEventEvents.emit(event + ':' + doc._id, doc);
    UserToEventEvents.emit(event, doc);
  }
}

export default UserToEventEvents;
