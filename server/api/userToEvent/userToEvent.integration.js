'use strict';

var app = require('../..');
import request from 'supertest';

var newUserToEvent;

describe('UserToEvent API:', function() {

  describe('GET /api/userToEvents', function() {
    var userToEvents;

    beforeEach(function(done) {
      request(app)
        .get('/api/userToEvents')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          userToEvents = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      userToEvents.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/userToEvents', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/userToEvents')
        .send({
          name: 'New UserToEvent',
          info: 'This is the brand new userToEvent!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newUserToEvent = res.body;
          done();
        });
    });

    it('should respond with the newly created userToEvent', function() {
      newUserToEvent.name.should.equal('New UserToEvent');
      newUserToEvent.info.should.equal('This is the brand new userToEvent!!!');
    });

  });

  describe('GET /api/userToEvents/:id', function() {
    var userToEvent;

    beforeEach(function(done) {
      request(app)
        .get('/api/userToEvents/' + newUserToEvent._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          userToEvent = res.body;
          done();
        });
    });

    afterEach(function() {
      userToEvent = {};
    });

    it('should respond with the requested userToEvent', function() {
      userToEvent.name.should.equal('New UserToEvent');
      userToEvent.info.should.equal('This is the brand new userToEvent!!!');
    });

  });

  describe('PUT /api/userToEvents/:id', function() {
    var updatedUserToEvent;

    beforeEach(function(done) {
      request(app)
        .put('/api/userToEvents/' + newUserToEvent._id)
        .send({
          name: 'Updated UserToEvent',
          info: 'This is the updated userToEvent!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedUserToEvent = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedUserToEvent = {};
    });

    it('should respond with the updated userToEvent', function() {
      updatedUserToEvent.name.should.equal('Updated UserToEvent');
      updatedUserToEvent.info.should.equal('This is the updated userToEvent!!!');
    });

  });

  describe('DELETE /api/userToEvents/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/userToEvents/' + newUserToEvent._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when userToEvent does not exist', function(done) {
      request(app)
        .delete('/api/userToEvents/' + newUserToEvent._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
