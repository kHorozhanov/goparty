/**
 * Created by kostya on 25.05.16.
 */
'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var User = require('../user/user.model');
var Event = require('../event/event.model');

var UserToEventSchema = new mongoose.Schema({
  user: {type:mongoose.Schema.Types.ObjectId, ref:'User'},
  event: {type:mongoose.Schema.Types.ObjectId, ref:'Event'},
  status: {type: Boolean, default: false}
});

export default mongoose.model('UserToEvent', UserToEventSchema);
